
# alias
alias c="clear"
alias x="exit"
alias nv="nvim"
alias xa='exa'
alias dot='dotbare'
alias td='cd ~/Development/todorst'
alias nvc='cd ~/.config/nvim'

# vars
export VISUAL="nvim"
export EDITOR="nvim"

export KU_STUDENT_ID="6410500319"

export CHROME_EXECUTABLE="/usr/bin/google-chrome-stable"
export _JAVA_AWT_WM_NONREPARENTING=1

# source
#. "$HOME/.cargo/env"

# path
export PATH=/home/phusitsom/.local/bin:$PATH
